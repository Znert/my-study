/**
 * This file provides types for every uni-related data.
 */

export class Lecture {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public start : Date,
        public end : Date,
        public location : string,
        public location_url : string,
        public description : string,
        public lec_key : string,
    ) {}
}

export class Metatag {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public internal_name : string,
        public priority? : number,
        public display_value? : string,
        public detail? : string,
        public href? : string,
        public hidden? : string,
    ){}
}

export class Instance {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public id : string,
        public display_name : string,
        public course : string,
        public program : string,
        public branch : string,
        public module : string,
        public lecturers : string,
        public registration : any[], // Cannot use atm (only IDs given)
        public lectures : Lecture[],
        public ects : number,
        public details : string,
        public syllabaus : string,
        public hasGroups : boolean,
        public reqSTEOP : boolean,
        public type : string,
        public homepage : string,
        public metatags : Metatag[],
        public vitalTags : Metatag[],
        public isDone = false,
    ){
    }
}

export class Course {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public id : string,
        public name : string,
        public instances : string[],
    ){
    }
}

export class Module {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public id : string,
        public name : string,
        public courses : string[],
    ){}
}

export class Branch {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public id : string,
        public name : string,
        public modules : string[],
    ) {
    }
}

export class Term {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public id : string,
        public name : string,
        public branches : string[],
    ){}
}

export class Program {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public id : string,
        public name : string,
        public terms : Term[],
    ){
    }
}
