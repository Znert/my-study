import * as t from './uni-data.types';

/**
 * This file provides types for all User related classes.
 */

export class Category {
    [key:string] : any; // allow arbitrary other meta data
    constructor(
        public id : string,
        public name : string,
        public order : number,
        public instances : t.Instance[],
        public isActive : boolean = false,
        public isMandatory : boolean = false,
    ){
    }
}

export class User {
    public categories : Category[] = [];
    public pending : t.Instance[] = [];
}
