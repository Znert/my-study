import { Observable } from 'rxjs/Observable';
import { Instance, Category } from './index';

/**
 * This file provides classes, that are implemented by components.
 */


/**
 * Abstract class for components, that allow managing categories.
 */
export abstract class CategoryManagerComp {
    public abstract removeInstance(inst : Instance) : any;
    public abstract getCurrentCat() : Category;
}


/**
 * Abstract class for components, that provide means to force pipes to update.
 */
export abstract class RerunPipesComp {
    public abstract getRerunObs() : Observable<any>;
    public abstract getRerun() : any;
    public abstract rerunPipes() : void;
}
