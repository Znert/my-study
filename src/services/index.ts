export * from './user/user.service';
export * from './uni-data/category.service';
export * from './uni-data/program.service';
export * from './uni-data/module.service';
export * from './uni-data/branch.service';
export * from './uni-data/course.service';
export * from './uni-data/instance.service';
export * from './demo/demo.service';

// This file will export all services so that importing is much easier and
// robust
