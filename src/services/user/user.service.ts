import { Injectable, isDevMode } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import { User, Category, Instance } from '../../types/index';
import { user } from '../../mock/user.mock';

import 'rxjs/add/operator/take';

/**
 * This service handles basic user manipulation as well as getting the current
 * user.
 */
@Injectable()
export class UserService {

    // The user as Observable
    private user$ = new ReplaySubject<User>(1);

    // The acutal user
    private _user : User;

    constructor() {

    }

    /**
     * This function will get the user in form of an Observable to track any
     * changes to the user.
     *
     * If the suer has not yet been initialised, intialisation is done first.
     *
     * @return The observable user.
     */
    public getUser() : Observable<User> {
        if (!this._user) {

            // If the user is not yet set, generate a new one and set the first
            // mandatory category
            this._user = new User();

            if(this._user.categories.length == 0) {
                this._user.categories = [
                    new Category(
                        "0",
                        "Mein 1. Semester",
                        0,
                        [],
                        true,
                        true,
                    )
                ];
            }

            this.user$.next(this._user);
        }

        return this.user$;
    }

    /**
     * This function will add an instance to the inbox.
     *
     * Not checks are done for the given instance.
     *
     * @param  inst The instance to add
     */
    public addToInbox(inst : Instance) {
        this.getUser().take(1).subscribe(
            user => {
                user.pending.push(inst);
            },
            err => {
                console.log("Failed to fetch user: ", err);
            }
        );
    }

    /**
     * This function will remove an instance from the inbox if it is currently
     * contained.
     *
     * @param  inst The instance to remove.
     */
    public removeFromInbox(inst : Instance) {
        this.getUser().take(1).subscribe(
            user => {
                let index = user.pending.findIndex(elem => elem.id === inst.id);
                if (index > -1) {
                    user.pending.splice(index, 1);
                    this.user$.next(user);
                }
            },
            err => {
                console.log("Failed to fetch user: ", err);
            }
        )
    }

    /**
     * This function will set all categories for the user.
     *
     * As this is a big change to the user, the changed user is pushed to the
     * user observable.
     *
     * @param  newCats The categories to set.
     */
    public setCats(newCats : Category[]) {
        this.getUser().take(1).subscribe(
            user => {
                user.categories.splice(0, user.categories.length, ...newCats);
                this.user$.next(user);
            },
            err => {
                console.log("Failed to fetch user: ", err);
            }
        )
    }

    /**
     * This function will set all instances in the inbox.
     *
     * @param  newInsts The instances to set.
     */
    public setInbox(newInsts : Instance[]) {
        this.getUser().take(1).subscribe(
            user => {
                user.pending.splice(0, user.pending.length, ...newInsts);
                this.user$.next(user);
            },
            err => {
                console.log("Failed to fetch user: ", err);
            }
        )
    }

    /**
     * This function will return the current inbox of the user as an observable.
     *
     * The observalbe will only fire once and is closed afterwards.
     *
     * @return The observalbe inbox.
     */
    public getInbox() : Observable<Instance[]> {
        let obs = new ReplaySubject<Instance[]>(1);
        this.getUser().take(1).subscribe(
            user => {
                obs.next(user.pending);
                obs.complete();
            },
            err => {
                console.log("Failed to fetch user: ", err);
            }
        )
        return obs;
    }

}
