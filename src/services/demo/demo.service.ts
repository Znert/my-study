import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map'

import { UserService, InstanceService, CategoryService } from '../index';

import { Category, RerunPipesComp } from '../../types/index';

/**
 * This service handles the logic for demoing the app.
 *
 * For the demo, it will reset the user and set the categories as well as all
 * instances in them. For the content to load, the server will be contacted.
 *
 * This service can also simulate the passing of a term. This is done by setting
 * all instances in mandatory categories to done and creating a new mandatory
 * category.
 */
@Injectable()
export class DemoService {

    constructor(
        private userService : UserService,
        private http : HttpClient,
        private instService : InstanceService,
        private catService : CategoryService,
    ) {

    }

    /**
     * This function will load the demo data from the server into the user.
     *
     * Because the data is server side stored as DemoData, it has to be parsed
     * first. This means, replacing all IDs for the instances with acutal
     * references.
     */
    public loadDemo() {
        let getDemoData = () => {
            this.http.get<DemoData>('src/mock/demo-mode.json').subscribe(
                data => {
                    Observable.forkJoin(
                        data.user.pending.map(p => this.instService.getByID(p).take(1)) // map all IDs from the inbox to Observables
                    ).subscribe(
                        data => {
                            // Data is now an array of instances.
                            this.userService.setInbox(data);
                        },
                        err => {
                            console.log("Error while fetching pending demo instances:", err);
                        }
                    )
                    Observable.forkJoin(
                        data.user.categories.map(cat => { // each category has to be filled
                            return Observable.forkJoin(
                                cat.instances.map(i => this.instService.getByID(i).take(1)), // each instance ID in a category is mapped to an Observable
                            ).map(
                                data => {
                                    // data is an array of instances
                                    let newCat = new Category(
                                        cat.id,
                                        cat.name,
                                        cat.order,
                                        data,
                                        cat.isActive,
                                        cat.isMandatory,
                                    );

                                    if (cat.isComplete) {
                                        for (let inst of newCat.instances) {
                                            inst.isDone = true;
                                        }
                                    }

                                    return newCat;
                                }
                            )
                        })
                    ).subscribe(
                        data => {
                            // data is an array of categories
                            this.userService.setCats(data);
                        },
                        err => {
                            console.log("Error while fetching instances for categories:", err);
                        }
                    )
                },
                err => {
                    console.log("Error while getting demo: ", err);
                }
            )
        }

        this.catService.getCats().take(1).subscribe(
            data => {
                for (let cat of data) {
                    for (let inst of cat.instances) {
                        inst.isDone = false;
                    }
                }
                getDemoData();
            }
        );
    }

    /**
     * This function will simulate the passing of a term.
     *
     * To do so, all instances in mandatory instances will be set to done and
     * a new mandatory category will be crated.
     *
     * @param  updater The update to call after the user has been updated.
     */
    public simulateTerm(updater : RerunPipesComp[] = []) {
        this.catService.getCats().take(1).subscribe(
            cats => {
                let mandatoryCount = 0;
                let lowestNotmandatory : number | undefined = undefined;
                let highestMandatory : number | undefined = undefined;
                for (let cat of cats) {
                    if (!cat.isMandatory) {
                        if (lowestNotmandatory === undefined) {
                            lowestNotmandatory = cat.order;
                        } else if (lowestNotmandatory > cat.order) {
                            lowestNotmandatory = cat.order;
                        }
                        continue;
                    }
                    mandatoryCount++;
                    if (highestMandatory === undefined) {
                        highestMandatory = cat.order;
                    } else if(highestMandatory < cat.order) {
                        highestMandatory = cat.order;
                    }
                    for (let inst of cat.instances) {
                        inst.isDone = true;
                    }
                    cat.isActive = false;
                }
                let newCat = new Category(
                    "" + cats.length,
                    "Mein " + (mandatoryCount + 1) + ". Semester",
                    ((lowestNotmandatory || 0) + (highestMandatory || 0)) / 2,
                    [],
                    true,
                    true,
                );
                this.catService.addCat(newCat);

                for (let u of updater) {
                    u.rerunPipes();
                }
            }
        )
    }
}

interface DemoData {
    user : {
        pending : string[],
        categories:{
            id: string,
            name: string,
            order: number,
            instances: string[],
            isActive: boolean,
            isMandatory : boolean,
            isComplete : boolean,
        }[],
    }
}
