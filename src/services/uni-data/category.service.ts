import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { UserService } from '../index';

import { Category } from '../../types/index';

/**
 * This service handles managing categories for a user.
 */
@Injectable()
export class CategoryService {

    private cats : Category[];
    private cats$ = new ReplaySubject<Category[]>(1);

    constructor(
        private userService : UserService,
    ){
        this.userService.getUser().subscribe(
            data => {
                this.cats = data.categories;
                this.cats$.next(this.cats);
            }
        )
    }

    /**
     * This function will return the categories in form of an Observable.
     *
     * @return An Observable publishing the categories.
     */
    public getCats() : ReplaySubject<Category[]> {
        return this.cats$;
    }

    /**
     * This function allows getting a category by an ID.
     *
     * @param  id The ID to search for.
     * @return    An Observable that will return the found category or undefined.
     */
    public getById(id : string) : Observable<Category> {
        let obs = new ReplaySubject<Category>(1);
        this.cats$.take(1).subscribe(
            data => {
                let found = data.find(cat => cat.id == id);
                obs.next(<Category>found);
                obs.complete();
            }
        )
        return obs;
    }

    /**
     * This function allows adding of categories to the user.
     *
     * @param  cat The category to add. Must not be undefined or null.
     */
    public addCat(cat : Category) {
        this.cats.push(cat);
        this.cats = this.cats.slice(); // clone the current categories
        this.userService.setCats(this.cats);
        this.cats$.next(this.cats);
    }

    /**
     * Removes a category from the user.
     *
     * @param  cat The category to remove.
     */
    public removeCat(cat : Category) {
        let index = this.cats.findIndex(elem => elem.id === cat.id);
        if (index < 0) {
            return;
        }
        this.cats.splice(index, 1);
        this.cats = this.cats.slice(); // clone the current categories.
        this.userService.setCats(this.cats);
        this.cats$.next(this.cats);
    }
}
