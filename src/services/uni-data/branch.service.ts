import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { Branch } from '../../types/index';

/**
 * This service handles getting branches from the server
 */
@Injectable()
export class BranchService {
    private static _branchesIndex : { [branch_key : string] : Branch } = {};

    constructor(private http : HttpClient) {

    }

    /**
     * This function will get the branch with given ID from the server.
     *
     * The ID has to exists, as to limitations of the backend server, index.html
     * may be returned with status 200.
     *
     * @param  id The ID of a branch.
     * @return    An Observable for the request.
     */
    public getByID(id : string) : Observable<Branch> {
        let branch = BranchService._branchesIndex[id];
        if (!branch) {
            let obs = new Subject<Branch>();
            this.http.get<Branch>("src/mock/branches/" + id + ".json")
                .subscribe(
                    data => {
                        BranchService._branchesIndex[id] = data;

                        obs.next(data);
                        obs.complete();
                    },
                    err => {
                        console.log("Failed to get branch from server: ", err);
                        obs.error(err);
                        obs.complete();
                    }
                );
            return obs;
        }

        return Observable.of(branch);
    }
}
