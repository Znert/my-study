import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { Course } from '../../types/index';

/**
 * This service handles getting courses from the server.
 *
 * A course is a grouping for several instances with the same content.
 */
@Injectable()
export class CourseService {
    private static _courseIndex : { [branch_key : string] : Course } = {};

    constructor(private http : HttpClient) {

    }

    /**
     * This function will get the course with given ID from the server.
     *
     * The ID has to exists, as to limitations of the backend server, index.html
     * may be returned with status 200.
     *
     * @param  id The ID of a course.
     * @return    An Observable for the request.
     */
    public getByID(id : string) : Observable<Course> {
        let course = CourseService._courseIndex[id];
        if (course) {
            return Observable.of(course);
        }

        let obs = new Subject<Course>();
        this.http.get<Course>("src/mock/courses/" + id + ".json")
            .subscribe(
                data => {
                    CourseService._courseIndex[id] = data;

                    obs.next(data);
                    obs.complete();
                },
                err => {
                    console.log("Failed to get course from server:", err);
                    obs.error(err);
                    obs.complete();
                }
            );

        return obs;
    }
}
