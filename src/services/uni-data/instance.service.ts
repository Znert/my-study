import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { Instance, Lecture } from '../../types/index';

/**
 * This service handles getting instances from the server
 */
@Injectable()
export class InstanceService {
    private static _instIndex : { [branch_key : string] : Instance } = {};

    constructor(private http : HttpClient) {

    }

    /**
     * This function will get the instance with given ID from the server.
     *
     * The ID has to exists, as to limitations of the backend server, index.html
     * may be returned with status 200.
     *
     * @param  id The ID of an instance.
     * @return    An Observable for the request.
     */
    public getByID(id : string) : Observable<Instance> {
        let inst = InstanceService._instIndex[id];
        if (inst) {
            return Observable.of(inst);
        }

        let obs = new Subject<Instance>();
        this.http.get<Instance>("src/mock/instances/" + id + ".json")
            .subscribe(
                data => {

                    for (let lec of data.lectures) {
                        lec.start = new Date(lec.start);
                        lec.end = new Date(lec.end);
                    }

                    InstanceService._instIndex[id] = data;

                    obs.next(data);
                    obs.complete();
                },
                err => {
                    console.log("Failed to get instance from server:", err);
                    obs.error(err);
                    obs.complete();
                }
            );

        return obs;
    }
}
