import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { Program } from '../../types/index';

/**
 * This service handles getting programs from the server
 */
@Injectable()
export class ProgramService {
    private static _progs : Program[];
    private static progs$ = new ReplaySubject<Program[]>(1);

    constructor(
        private http : HttpClient
    ) {

    }

    /**
     * This function will get all known programs from the server.
     *
     * @return An Observable for the request.
     */
    public getPrograms() : Observable<Program[]> {
        if (!ProgramService._progs) {
            this.http.get<Program[]>("src/mock/programs/all-programs.json")
                .subscribe(
                    data => {
                        ProgramService._progs = data;
                        ProgramService.progs$.next(ProgramService._progs);
                    },
                    err => {
                        console.log("Failed to get programs from server: ", err);
                    }
                );
        }

        return ProgramService.progs$;
    }
}
