import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { Module } from '../../types/index';

/**
 * This service handles getting modules from the server
 */
@Injectable()
export class ModuleService {
    private static _modIndex : { [branch_key : string] : Module } = {};

    constructor(private http : HttpClient) {

    }

    /**
     * This function will get the module with given ID from the server.
     *
     * The ID has to exists, as to limitations of the backend server, index.html
     * may be returned with status 200.
     *
     * @param  id The ID of a module.
     * @return    An Observable for the request.
     */
    public getByID(id : string) : Observable<Module> {
        let mod = ModuleService._modIndex[id];
        if (mod) {
            return Observable.of(mod);
        }

        let obs = new Subject<Module>();
        this.http.get<Module>("src/mock/modules/" + id + ".json")
            .subscribe(
                data => {
                    ModuleService._modIndex[id] = data;

                    obs.next(data);
                    obs.complete();
                },
                err => {
                    console.log("Failed to get branch from server: ", err);
                    obs.error(err);
                    obs.complete();
                }
            );

        return obs;
    }
}
