/**
 * Simple function to convert px to rem unit.
 * @param  px The px to convert.
 */
export function pixelToRem(px : number) : number {
    let oneRem = parseFloat(<string> getComputedStyle(document.documentElement).fontSize);
    return px / oneRem;
}
