import { Component, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/of';

import { ProgramService, BranchService, ModuleService, CourseService, InstanceService, UserService } from '../../services/index';

import { Program, Term, Branch, Module, Course, Instance } from '../../types/index';

import { growInOut } from '../../animations/animations';

/**
 * This component provides means to select instances and add them to the inbox.
 */
@Component({
    selector : 'instance-picker',
    templateUrl : 'src/components/instance-picker/instance-picker.component.html',
    styleUrls : [
        'src/components/instance-picker/instance-picker.component.css',
    ],
    animations : [
        growInOut(),
    ],
})
export class InstancePickerComponent {
    private programs$ : Observable<Program[]>;

    private branchIndex : { [branch_id : string] : Branch } = {};

    private branches : { [term_id : string] : Branch[] } = {};
    private modules : { [branch_id : string] : Module[] } = {};
    private courses : { [mod_id : string] : Course[] } = {};
    private instances : { [course_id: string] : Instance[] } = {};

    constructor(
        private cd : ChangeDetectorRef,
        private progService : ProgramService,
        private branchService : BranchService,
        private moduleService : ModuleService,
        private courseService : CourseService,
        private instanceService : InstanceService,
        private userService : UserService,
    ) {
        this.programs$ = this.progService.getPrograms();
    }

    private toggleProg(prog : Program) {
        prog["expanded"] = !prog["expanded"];
    }

    private expandTerm(term : Term) {
        term["expanded"] = !term["expanded"];

        if (term["expanded"] && !this.branches[term.id]) {
            Observable.forkJoin(term.branches.map(mod => {
                return this.branchService.getByID(mod);
            })).subscribe(
                data => {
                    this.branches[term.id] = data;
                },
                err => {
                    console.log("Error while fetching modules: ", err);
                }
            )
        }
    }

    private toggleBranch(branch : Branch) {
        branch["expanded"] = !branch["expanded"];

        if (branch["expanded"] && !this.modules[branch.id]) {
            Observable.forkJoin(branch.modules.map(mod => {
                return this.moduleService.getByID(mod);
            })).subscribe(
                data => {
                    this.modules[branch.id] = data;
                },
                err => {
                    console.log("Error while fetching modules: ", err);
                }
            )
        }
    }

    private toggleModule(mod : Module) {
        mod["expanded"] = !mod["expanded"];

        if (mod["expanded"] && !this.courses[mod.id]) {
            Observable.forkJoin(mod.courses.map(course => {
                return this.courseService.getByID(course);
            })).subscribe(
                data => {
                    this.courses[mod.id] = data;
                },
                err => {
                    console.log("Error while fetching courses: ", err);
                }
            )
        }
    }

    private toggleCourse(course : Course) {
        course["expanded"] = !course["expanded"];

        if (course["expanded"] && !this.instances[course.id]) {
            Observable.forkJoin(course.instances.map(i => {
                return this.instanceService.getByID(i);
            })).subscribe(
                data => {
                    this.instances[course.id] = data;
                },
                err => {
                    console.log("Error while fetching instances: ", err);
                }
            )
        }
    }

    private toggleInbox(inst : Instance) {
        this.userService.getInbox().take(1).subscribe(
            inbox => {
                if (inbox.find(elem => inst.id === elem.id)) {
                    this.userService.removeFromInbox(inst);
                } else {
                    this.userService.addToInbox(inst);
                }
            }
        )
    }

    private pendingFilter(inst : Instance) {
        if (!inst) {
            return () => false;
        }
        return (elem : Instance) => elem ? elem.id === inst.id : false
    }
}
