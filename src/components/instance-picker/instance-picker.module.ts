import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomPipesModule } from '../../pipes/pipes.module';

import { InstancePickerComponent } from './instance-picker.component';

@NgModule({
    imports : [
        CommonModule,
        CustomPipesModule,
    ],
    declarations : [
        InstancePickerComponent,
    ],
    exports : [
        InstancePickerComponent,
    ]

})
export class InstancePickerModule {

}
