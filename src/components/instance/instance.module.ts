import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InstanceComponent } from './instance.component';

import { CustomPipesModule } from '../../pipes/pipes.module';

@NgModule({
    imports : [
        CommonModule,
        CustomPipesModule,
        FormsModule,
    ],
    declarations : [
        InstanceComponent,
    ],
    exports : [
        InstanceComponent,
    ]
})
export class LvaModule {}
