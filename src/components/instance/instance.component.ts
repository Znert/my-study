import { Component, Input, ViewChild, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { pixelToRem } from '../../util/pxToRem';

import { CategoryService } from '../../services/index';

import { Instance, Lecture, CategoryManagerComp , Category} from '../../types/index';

import { growInOut } from '../../animations/animations';

/**
 * This component displays a single instance.
 *
 * It provides a expandable view, which provides extensive information about the
 * instance. Metatags provide information while the instance is collapsed.
 */
@Component({
    selector: 'instance',
    templateUrl : 'src/components/instance/instance.component.html',
    styleUrls : [
        "src/components/instance/instance.component.css",
    ],
    animations : [
        growInOut(),
    ],
})
export class InstanceComponent {
    @Input()
    private inst : Instance;

    private expanded = false;

    private swap = false;

    private set nullValue(ignore : any) {
    }
    private get nullValue() {
        return null;
    }

    constructor(
        private catManager : CategoryManagerComp,
        private catService : CategoryService,
    ){

    }

    /**
     * Expand/collapse the instance.
     */
    private toggleContent() {
        this.expanded = !this.expanded;
    }

    /**
     * Toggles which view is displayed.
     *
     * Is only revelant for mobile view.
     */
    private toggleSwap() {
        this.swap = !this.swap;
    }

    /**
     * Automatically collapse, when a drag starts.
     */
    @HostListener('dragstart', ['$event'])
    private onDrag(e : DragEvent) {
        this.expanded = false;
    }

    /**
     * Converts two dates in an as compact as possible string.
     * @param  start The first date to display. Has to be before the second date.
     * @param  end   The second date to display. Has to be after the first date.
     */
    private convertDates(start : Date, end : Date) {
        let dateString = start.getDate() + "." + (start.getMonth() + 1) + "." + (start.getFullYear() % 100)
            + " " + (start.getHours() < 10 ? "0" : "") + start.getHours() + ":"
            + (start.getMinutes() < 10 ? "0" : "") + start.getMinutes() + " - ";
        if (start.getFullYear() == end.getFullYear()
            && start.getMonth() == end.getMonth()
            && start.getDate() == end.getDate()) {
            dateString += (end.getHours() < 10 ? "0" : "") + end.getHours() + ":"
                + (end.getMinutes() < 10 ? "0" : "") + end.getMinutes();
        } else {
            dateString += end.getDate() + "." + (end.getMonth() + 1) + "." + (end.getFullYear() % 100)
                + " " + (end.getHours() < 10 ? "0" : "") + end.getHours() + ":"
                + (end.getMinutes() < 10 ? "0" : "") + end.getMinutes();
        }
        return dateString;
    }

    /**
     * This function will set the id of this instance to the drag event.
     * @param  e The event that caused this function to be called.
     */
    private drag(e : DragEvent) {
        e.dataTransfer.setData('inst', this.inst.id);
    }

    /**
     * Comparator function for sorting the lecturs.
     * @param  a The first lecture.
     * @param  b The second lecture.
     * @return   Negative number if a is after b, 0 if equal, postive elsewise.
     */
    private lecSortFunc(a : Lecture, b : Lecture) {
        return a.start.getTime() - b.start.getTime() || a.end.getTime() - b.end.getTime();
    }

    /**
     * This function will remove this instance from the category it is currently
     * in.
     */
    private removeInstance() {
        if (!confirm("Diese LVA aus deinem Bereich entfernen?")) {
            return;
        }
        this.catManager.removeInstance(this.inst);
    }

    /**
     * Moves this instance to a new category.
     * @param  cat The category to move to.
     */
    private moveInstTo(cat : Category) {
        this.catManager.removeInstance(this.inst);
        cat.instances.push(this.inst);
        cat.instances = cat.instances.slice();
    }

    /**
     * Prevents an event from bubbling.
     * 
     * @param  event The event to prevent bubbling.
     */
    private stopBubble(event : MouseEvent) {
        event.stopPropagation();
    }

}
