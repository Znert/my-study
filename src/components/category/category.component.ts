import { Component, Input, ViewChild, ElementRef, OnInit, ChangeDetectorRef, Injectable } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Category, Instance, CategoryManagerComp, RerunPipesComp } from '../../types/index';
import { pixelToRem } from '../../util/pxToRem';
import { CategoryService, InstanceService } from '../../services/index';
import { Observable } from 'rxjs/Observable';

import { fadeInOut, fadeOut, growInOut } from '../../animations/animations';

/**
 * This component represents one category in the my-study part.
 *
 * It contains instances and presents them within an collapseable/expandable
 * container. While expanded, settings for the category can be changed (only
 * name and "active" actually has an effect) or deleted.
 */
@Injectable()
@Component({
    selector : 'category',
    templateUrl : 'src/components/category/category.component.html',
    styleUrls : [
        "src/components/category/category.component.css",
    ],
    animations : [
        fadeInOut(200, .9),
        growInOut(),
        fadeOut(),
    ],
    providers: [
        // Provide itself as Category Manager to child components
        {
            provide: CategoryManagerComp,
            useExisting: CategoryComponent,
        }
    ]
})
export class CategoryComponent extends CategoryManagerComp implements OnInit {
    // The category to display
    @Input()
    private category : Category;

    // Reference to the instance-container; required for in-HTML-code
    @ViewChild("instanceContainer")
    private instanceContainer : ElementRef;

    // Boolean to expand/collapse category
    private expanded = false;

    // String for orderBy pipe for instances
    private sortParams = "name type";
    // Boolean to toggle order direction; currently not used
    private sortDecending = false;

    // Boolean to show/hide settings
    private showSettings = false;

    // Boolean to show/hide hint in settings
    private showHint_activeCat = false;

    // Counter to detect if user is currently dragging over category
    private peek = 0;

    // Ovservable to rerun pipes
    private rerunObs : Observable<any>;

    constructor(
        private catService : CategoryService,
        private instService : InstanceService,
        // This category requires a RerunPipesComp instead of the my-study
        // component to improve loose coupling
        private rerunComp : RerunPipesComp,
    ) {
        super();
        this.rerunObs = rerunComp.getRerunObs();
    }

    /**
     * Method for removing an instance from this cateogry.
     * @param  inst The instance to remove.
     */
    public removeInstance(inst : Instance) {
        let index = this.category.instances.findIndex(i => i.id == inst.id);
        if (index > -1) {
            this.category.instances.splice(index, 1);
            this.rerunComp.rerunPipes();
        }
    }

    /**
     * Method for getting the reference to the cateogry being displayed.
     * @return The current cateogry
     */
    public getCurrentCat() {
        return this.category;
    }

    /**
     * Called when the component is built.
     *
     * This function will set the expanded toggle to true, if the category is
     * active.
     */
    public ngOnInit() {
        this.expanded = this.category.isActive;
    }

    /**
     * This function is used for the filter pipe.
     *
     * It will return true for all (not undefined) instances, which are done.
     *
     * @param  inst [description]
     * @return      [description]
     */
    private filterDoneFn(inst : Instance) {
        return inst ? inst.isDone : false;
    }

    /**
     * This function will return the sum of ECTS currently in the category
     *
     * @param  cat The category to get the ECTS from.
     * @return     The sum of ECTS in this cateogry.
     */
    private getECTS(cat : Category) : number {
        return cat.instances.reduce((a,b) => a + b.ects, 0);
    }

    /**
     * This function will return the sum of hours per week in this cateogry.
     *
     * It is currently only implemented as dummy and will only return the number
     * of instances in this cateogry time 1.5.
     *
     * @param  cat The category to get the hours for.
     * @return     The sum of hours per week in this category.
     */
    private getHours(cat : Category) : number {
        return cat.instances.length * 1.5;
    }

    /**
     * This function will expand/collapse the category.
     *
     * It is called when the category header is clicked.
     */
    private toggleExpand() {
        this.expanded = ! this.expanded;
        this.showSettings = false;
        this.showHint_activeCat = false;
    }

    /**
     * This function will show/hide the settings for this cateogry.
     *
     * It is called when clicked on the gear icon.
     */
    private toggleSettings() {
        this.showSettings = !this.showSettings;
        this.showHint_activeCat = false;
    }

    /**
     * This function will return the minimum of two given numbers.
     *
     * It is required to use it in the HTML template.
     *
     * @param  a Number to compare.
     * @param  b Number to compare.
     * @return   The minimum of a and b.
     */
    private min(a : number, b : number) {
        return Math.min(a,b);
    }

    /**
     * Function to set the category ID to the drag event.
     *
     * This ID can then be used to geht this category.
     *
     * @param  e The drag event that caused this function to be called.
     */
    private drag(e : DragEvent) {
        e.dataTransfer.setData('cat_id', this.category.id);
    }

    /**
     * This function will allow the user to drop an instance into this category.
     *
     * @param  e The drag event that caused this function to be called.
     */
    private allowDrop(e : DragEvent) {
        e.preventDefault();
    }

    /**
     * This function will track, if the user is currently dragging over this
     * category.
     */
    private dragEnter() {
        this.peek++;
    }
    /**
     * This function will track, if the user is currently dragging over this
     * category.
     */
    private dragLeave() {
        this.peek--;
    }

    /**
     * This function will handle dropping an instance onto this category.
     *
     * The instance is moved to this category and removed from the old one.
     *
     * To do so, the instance ID and category ID will be extracted from the drag
     * event.
     *
     * @param  e The drag event that caused this function to be called.
     */
    private drop(e : DragEvent) {
        this.peek = 0;
        let instId = e.dataTransfer.getData('inst');
        let othterId = e.dataTransfer.getData('cat_id');
        if (othterId == this.category.id) {
            return;
        }

        this.instService.getByID(instId).subscribe(
            newLVA => {
                this.category.instances.push(newLVA);

                // Clone array, to trigger pipes
                this.category.instances = this.category.instances.slice();

            }
        );
        this.catService.getById(othterId).subscribe(
            otherCat => {
                if (otherCat) {
                    let index = otherCat.instances.findIndex(elem => elem.id === instId);
                    if (index > -1) {
                        otherCat.instances.splice(index, 1);
                        otherCat.instances = otherCat.instances.slice();
                    }
                }
            },
        );

    }

    /**
     * This function will remove the category from the user.
     *
     * It is called when clicking the remove button in the settings.
     */
    private removeCat() {
        if (this.category.instances.length != 0) {
            if (!confirm("Alle LVAs in dieser Kategorien werden gelöscht.\nFortfahren?")) {
                return;
            }
        }
        this.catService.removeCat(this.category);
    }
}
