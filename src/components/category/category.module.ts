import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CategoryComponent } from './category.component';
import { LvaModule } from '../instance/instance.module';

import { CustomPipesModule } from '../../pipes/pipes.module';

/**
 * This module will export the Category Component.
 */
@NgModule({
    imports : [
        CommonModule,
        LvaModule,
        CustomPipesModule,
        FormsModule,
    ],
    declarations : [
        CategoryComponent,
    ],
    exports : [
        CategoryComponent,
    ],
})
export class CategoryModule {
}
