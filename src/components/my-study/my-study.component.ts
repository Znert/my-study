import { Component, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { slideOutRight, fadeInOut, delayDestroy, scaleInOut } from '../../animations/animations';
import { UserService, CategoryService } from '../../services/index';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import { Category, User, Instance, RerunPipesComp } from '../../types/index';

/**
 * This component represents the individual area for each student.
 *
 * Here students can categorize their courses (see CategoryComponent) and get
 * information about them (see LvaComponent).
 */
@Component({
    selector : 'my-study',
    templateUrl : 'src/components/my-study/my-study.component.html',
    styleUrls : [
        "src/components/my-study/my-study.component.css",
    ],
    animations : [
        slideOutRight(400),
        fadeInOut(),
        delayDestroy(400),
        scaleInOut(400),
        // add custom animations for showing/hiding the add category form
        trigger('tagAnimation', [
            transition(':enter', [
                animate(200, keyframes([
                    style({
                        width : 0,
                        "border-top-right-radius" : '5rem',
                        "border-bottom-right-radius" : '5rem',
                        transform : 'scale(.5)',
                        offset: 0
                    }),
                    style({
                        "border-top-right-radius" : "5rem",
                        "border-bottom-right-radius" : "5rem",
                        transform : 'scale(1)',
                        offset : 0.1,
                    }),
                    style({
                        "border-top-right-radius" : 0,
                        "border-bottom-right-radius" : 0,
                        width : '*',
                        offset : 1.0,
                    }),
                ])),
            ]),
            transition(':leave', [
                animate(200, keyframes([
                    style({
                        "border-top-right-radius" : 0,
                        "border-bottom-right-radius" : 0,
                        width : '*',
                        offset: 0
                    }),
                    style({
                        "border-top-right-radius" : "5rem",
                        "border-bottom-right-radius" : "5rem",
                        transform : 'scale(1)',
                        width: 0,
                        offset : 0.9,
                    }),
                    style({
                        width : 0,
                        "border-top-right-radius" : '5rem',
                        "border-bottom-right-radius" : '5rem',
                        transform : 'scale(.5)',
                        offset : 1.0,
                    }),
                ])),
            ]),
        ])
    ],
    // This tag will export the current component so that child components can
    // inject "RerunPipesComp"
    providers: [
        {
            provide: RerunPipesComp,
            useExisting: MyStudyCompoent,
        }
    ]
})
export class MyStudyCompoent extends RerunPipesComp implements OnDestroy {

    // Map of expaned categories
    private expandedCats : { [catId : string] : boolean} = {};

    // Boolean for showing/hiding the trash icon at the top of the site
    // Will be shown, if user is currently holding and draging a course
    private showDelete = false;

    // Boolean for showing/hiding the inbox
    private showInbox = false;

    // Boolean for showing/hiding the category creator
    private showCatCreator = false;
    // Variable for mapping the input field
    private newCatname : string;

    // This setter and getter are required for ngModel in the inbox
    // It will prevent that any real value is set, but will still fire the
    // ngModelChange event.
    private set nullValue(ignore : any) {
    }
    private get nullValue() {
        return null;
    }

    // The current user to display
    private user : User = new User();

    // Boolean for showing/hiding the overlay over the category
    private showOverlay = false;
    // Function to call when the overlay is clicked
    private onOverlayClick : () => any = () => {};

    // Value for mapping the dropbox for move all
    private inboxSelectedCat : Category;

    // Killswitch for all observables
    private subjectKillSwitch = new ReplaySubject<boolean>(1);

    // Observable to use as trigger for recalculating pipes
    private rerunObs = new Subject<number>();
    // Actual value of the trigger. Can be arbitrary
    private rerun = 0;

    constructor(
        private userService : UserService,
        private catService : CategoryService,
        private cd : ChangeDetectorRef,
    ) {
        super();
        this.userService.getUser().takeUntil(this.subjectKillSwitch).subscribe(
            user => {
                this.user = user;
            }
        )
    }

    /**
     * Function to execute when the component closes.
     */
    public ngOnDestroy() {
        this.subjectKillSwitch.next(true);
        this.subjectKillSwitch.complete();

        this.rerunObs.complete();
    }

    /**
     * Method for getting an Observable to use as trigger for recalculating all
     * pipes and other operations.
     *
     * @return A trigger for rerunning operations.
     */
    public getRerunObs() {
        return this.rerunObs;
    }

    /**
     * Method for getting the actual value of the trigger. If this value changes,
     * operations should be rerun.
     *
     * Can be arbitrary.
     *
     * @return Any value.
     */
    public getRerun() {
        return this.rerun;
    }

    /**
     * This method will force all pipes to rerun.
     *
     * This is used in cases where the reference to an object does not change,
     * but the result of the pipe might.
     */
    public rerunPipes() {
        this.rerunObs.next(++this.rerun);
    }

    /**
     * This function is called when the user clicked on the inbox.
     *
     * It will expand the inbox and show all courses currently in it.
     */
    private onInboxClicked() {
        if (this.showInbox = !this.showInbox) {
            // this.showInbox is true
            this.showOverlay = true;
            this.onOverlayClick = () => {
                this.showInbox = false;
                this.showOverlay = false;
                this.onOverlayClick = () => {};
            };
        } else {
            this.showOverlay = false;
            this.onOverlayClick = () => {};
        }
    }

    /**
     * This method is called, when the user selects a category from the
     * hover-only dropbox.
     *
     * It will move the given instance to given category and remove it from the
     * pending courses of the current user.
     *
     * @param  inst [description]
     * @param  cat  [description]
     */
    private moveInstTo(inst : Instance, cat : Category) {
        cat.instances.push(inst);
        cat.instances = cat.instances.slice();
        let index = this.user.pending.findIndex(elem => elem === inst);
        if (index > -1) {
            this.user.pending.splice(index, 1);
        }
    }

    /**
     * This function is called, when the user clicked on the move-all button.
     *
     * It will remove all courses from the inbox and move them to the currently
     * selected category.
     */
    private moveAll() {
        if (!this.inboxSelectedCat) {
            return;
        }
        this.inboxSelectedCat.instances = this.inboxSelectedCat.instances
            .concat(this.user.pending);
        this.user.pending = [];
    }

    /**
     * This function will create a new category and add it to the current user.
     *
     * It will also force all pipes to rerun.
     */
    private createNewCat() {
        let newCat = new Category(
            "" + this.user.categories.length,
            this.newCatname,
            this.user.categories.reduce((a,b) => a < b.order ? b.order : a, 0) + 1,
            [],
            false,
            false,
        );
        this.catService.addCat(newCat);
        this.rerunPipes();
        this.showCatCreator = false;

        if (newCat.isActive) {
            this.expandedCats[newCat.id] = true;
        }

        this.newCatname = "";
    }

    /**
     * This method will show the trash icon on the top of the site.
     */
    private showTrash() {
        this.showDelete = true;
        this.showCatCreator = false;
        this.showInbox = false;
    }

    /**
     * This method will hide the trash icon on the top of the site.
     */
    private hideTrash() {
        this.showDelete = false;
    }

    // Counter to keep track of how many time the user entered and left a
    // dragable area.
    private dragTrashcounter = 0;

    /**
     * This function is called every time the dragEnter event is fired.
     */
    private enterTrash() {
        this.dragTrashcounter++;
    }

    /**
     * This function is called every time the dragLeave event is fired.
     */
    private leaveTrash() {
        this.dragTrashcounter--;
    }

    /**
     * This function is called when the user drops an instance onto the trash.
     *
     * It will remove that course from the cateogry and therefore also from the
     * user after asking the user if he or she is sure.
     *
     * @param  e The drag event that caused dropTrash to be called.
     */
    private dropTrash(e : DragEvent) {
        let instId = e.dataTransfer.getData('inst');
        let catId = e.dataTransfer.getData('cat_id');

        this.catService.getById(catId).subscribe(
            cat => {
                if (!cat) return;
                let index = cat.instances.findIndex(elem => elem.id === instId);
                if (index > -1) {
                    if (!confirm("Diese LVA aus deinem Bereich entfernen?")) {
                        return;
                    }
                    cat.instances.splice(index, 1);
                    cat.instances = cat.instances.slice();
                }
            },
        );
        this.hideTrash()
    }

    /**
     * Function to allow dropping an element onto somewhere.
     * @param  e The drag event that caused that function to be called.
     */
    private allowDrop(e : DragEvent) {
        e.preventDefault();
    }
}
