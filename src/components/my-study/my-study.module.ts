import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MyStudyCompoent } from './my-study.component';
import { CategoryModule } from '../category/category.module';

import { CustomPipesModule } from '../../pipes/pipes.module';

@NgModule({
    imports : [
        CommonModule,
        CategoryModule,
        CustomPipesModule,
        FormsModule,
    ],
    declarations : [
        MyStudyCompoent,
    ],
    exports : [
        MyStudyCompoent,
    ],
})
export class MyStudyModule {
}
