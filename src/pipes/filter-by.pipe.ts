import { Pipe, PipeTransform } from '@angular/core';

/**
 * This pipe provides means to filter an array via giving a custom filter
 * function.
 *
 * It uses Array.filter and therfore the requirements for the function are the
 * same.
 */
@Pipe({
    name: 'filterBy',
})
export class FilterByPipe implements PipeTransform {
    public transform<T>(
        value : T[] | T,
        fn : (a : T, i : number, v : T[]) => boolean,
        trigger : any,
    ) : T[] {
        if (!value) return value;
        if (!Array.isArray(value)) {
            value = [value];
        }
        if (typeof fn != "function") {
            return value;
        }

        return value.filter(fn);
    }
}
