import { Pipe, PipeTransform } from '@angular/core';

interface KeyValueObject {
    [key : string] : any;
}

/**
 * This pipe implements sorting of an Array.
 *
 * There are two ways to define the ordering of the elements:
 *  As a function. It is passed to Array.sort and the decending parameter is
 *      ignored.
 *  As a string. In this case a new function will be generated, which compares
 *      the properties with given names of each object. Multiple properties can
 *      be compared by seperating them with a single space.
 */
@Pipe({
    name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {
    public transform<T extends KeyValueObject>(
        value : T[], propertyOrFunction : ((a : T, b : T) => number) | string,
        decending = false,
        trigger : any,
    ) : T[] {
        if (!value) return value;
        if (!Array.isArray(value)) return value;
        if (!propertyOrFunction) return value;

        if (typeof propertyOrFunction != 'string'){
            return value.sort(propertyOrFunction);
        }

        let criteria = propertyOrFunction.split(' ');

        if (!decending){
            return value.sort((a,b) => {
                for (let prop of criteria) {
                    if(a[prop] < b[prop]) {
                        return -1;
                    }
                    if (a[prop] > b[prop]) {
                        return +1;
                    }
                }
                return 0;
            });
        } else {
            return value.sort((a,b) => {
                for (let prop of criteria) {
                    if(a[prop] > b[prop]) {
                        return -1;
                    }
                    if (a[prop] < b[prop]) {
                        return +1;
                    }
                }
                return 0;
            });
        }

    }
}
