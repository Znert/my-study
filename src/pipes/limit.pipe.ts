import { Pipe, PipeTransform } from '@angular/core';

/**
 * This simple pipe will limit the amount of elements in an array.
 *
 * It uses Array.slice.
 */
@Pipe({
    name: 'limit',
})
export class LimitPipe implements PipeTransform {
    public transform<T>(
        value : T[], limit : number,
    ) : T[] {
        if (typeof limit != "number") return value;
        if (!Array.isArray(value)) return value;

        return value.slice(0, limit);
    }
}
