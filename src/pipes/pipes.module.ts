import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilterByPipe } from './filter-by.pipe';
import { OrderByPipe } from './order-by.pipe';
import { LimitPipe } from './limit.pipe';

/**
 * This module exports all pipes in the same folder.
 *
 * Components can therfore import only this component and have acces to all
 * custom pipes.
 */
@NgModule({
    imports : [
        CommonModule,
    ],
    declarations : [
        FilterByPipe,
        OrderByPipe,
        LimitPipe,
    ],
    exports : [
        FilterByPipe,
        OrderByPipe,
        LimitPipe,
    ]
})
export class CustomPipesModule {}
