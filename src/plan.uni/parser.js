/* jshint esversion: 6 */

/**
 * This parser will read the plan.uni mock and extract all different types of
 * data and put it into appropiate folders.
 *
 * Those folder can be moved to the mock folder so that the data can be
 * accessed by the services.
 */

console.log("Starting parser");

const fs = require('fs');

fs.readFile('plan.uni.mock.json', (err, data) => {
    if (err) {
        return console.log(err);
    }
    console.log("Bytes read: ", data.length);

    let temp = JSON.parse(data);

    console.log("Parser done");

    console.log("Starting mock extraction.");

    temp.programmes.sort((a,b) => {
        return a.name.localeCompare(b.name);
    });

    fs.mkdir("programs", () => {});
    fs.mkdir("branches", () => {});
    fs.mkdir("modules", () => {});
    fs.mkdir("courses", () => {});
    fs.mkdir("instances", () => {});

    let generatedPrograms = [];

    for (let prog of temp.programmes) {
        let newProg = {
            id : prog._id,
            name : prog.name,
            terms : [],
        };
        let year = 2017;
        let even = "SS";
        let odd = "WS";
        for (let i in prog.terms) {
            let term = prog.terms[i];
            let newTerm = {
                id : term._id,
                name : Math.floor(year + i/2) + (i % 2 == 0 ? even : odd),
                branches : [],
            };
            term.branches.sort((a,b) => {
                return a.name.localeCompare(b.name);
            });
            for (let branch of term.branches) {
                newTerm.branches.push(branch._id);
                let newBranch = {
                    id : branch._id,
                    name : branch.name,
                    modules : [],
                };
                branch.modules.sort((a,b) => {
                    return a.name.localeCompare(b.name);
                });
                for (let mod of branch.modules) {
                    newBranch.modules.push(mod._id);
                    let newMod = {
                        id : mod._id,
                        name : mod.name,
                        courses : [],
                    };
                    mod.courses.sort((a,b) => {
                        return a.name.localeCompare(b.name);
                    });
                    for (let course of mod.courses) {
                        // if (!course.instances || course.instances.length == 0) {
                        //     continue;
                        // }
                        newMod.courses.push(course._id);
                        let newCourse = {
                            id : course._id,
                            name : course.name,
                            instances : [],
                        };
                        course.instances.sort();
                        for (let inst of course.instances) {
                            newCourse.instances.push(inst._id);
                            let newInst = {
                                id : inst._id,
                                display_name : inst.display_name,
                                course : inst.course,
                                program : inst.programme,
                                branch : inst.branch,
                                module : inst.module,
                                lecturers : inst.lecturers,
                                registration : [], // Cannot use atm (only IDs given)
                                lectures : [],
                                metatags : [],
                                vitalTags : [],
                            };

                            /* Map meta data to new format */

                            newInst.ects = inst.metatags.ects.hidden;
                            newInst.vitalTags.push({
                                internal_name : 'ects',
                                display_value : inst.metatags.ects.display_value,
                                priority : inst.metatags.ectspriority,
                                hidden : inst.metatags.ects.hidden,
                            });

                            // TISS link
                            inst.metatags.details = { // rename metatag
                                display_value : 'Details', // rename
                                priority : inst.metatags.tiss.priority,
                                href : inst.metatags.tiss.href,
                            };
                            newInst.details = inst.metatags.tiss.href;
                            newInst.vitalTags.push({
                                internal_name : 'details',
                                display_value : 'Details',
                                priority : inst.metatags.tiss.priority,
                                href : inst.metatags.tiss.href,
                            });
                            delete inst.metatags.tiss; // remove old tag

                            if (inst.metatags.tuwel) {
                                newInst.tuwel = inst.metatags.tuwel.href;
                                newInst.vitalTags.push({
                                    internal_name : "tuwel",
                                    priority : inst.metatags.tuwel.priority,
                                    display_value : "TUWEL",
                                    href : inst.metatags.tuwel.href,
                                });
                            }

                            if (inst.metatags.homepage) {
                                newInst.homepage = inst.metatags.homepage.href;
                                newInst.vitalTags.push({
                                    internal_name : "homepage",
                                    priority : inst.metatags.homepage.priority,
                                    display_value : "Homepage",
                                    href : inst.metatags.homepage.href,
                                });
                            }

                            // Syllabaus
                            newInst.syllabaus = inst.metatags.syllbaus.hidden;
                            delete inst.metatags.syllbaus; // remove from metatags

                            newInst.type = inst.metatags.type.display_value;
                            delete inst.metatags.type;

                            newInst.hasGroups = !!inst.metatags.group; // Check if instance has groups

                            if (newInst.hasGroups) {
                                newInst.vitalTags.push({
                                    internal_name : "group",
                                    priority : inst.metatags.group.priority,
                                    display_value : inst.metatags.group.display_value,
                                    hidden: inst.metatags.group.hidden,
                                    href : inst.metatags.group.href,
                                });
                            }

                            if (inst.metatags.info) {
                                newInst.reqSTEOP = inst.metatags.info.display_value == "STEOP"; // Check if lva needs STEOP
                                newInst.vitalTags.push({
                                    internal_name : "info",
                                    priority : inst.metatags.info.priority,
                                    display_value : inst.metatags.info.display_value,
                                });
                            }

                            delete inst.metatags.updatetime; // Remove unneccessary metatag

                            newInst.literature = inst.metatags.literature.hidden;
                            delete inst.metatags.literature;

                            for (let key in inst.metatags) {
                                if (!inst.metatags[key].display_value) {
                                    continue;
                                }

                                let newMetatag = {
                                    internal_name : key,
                                    priority : inst.metatags[key].priority,
                                    display_value : inst.metatags[key].display_value,
                                    detail : inst.metatags[key].detail,
                                    href : inst.metatags[key].href,
                                    hidden : inst.metatags[key].hidden,
                                };
                                delete inst.metatags[key].priority;
                                delete inst.metatags[key].display_value;
                                delete inst.metatags[key].detail;
                                delete inst.metatags[key].href;
                                delete inst.metatags[key].hidden;
                                if (Object.keys(inst.metatags[key]) > 0) {
                                    console.log(inst.metatags[key]);
                                    return;
                                }
                                newInst.metatags.push(newMetatag);
                            }
                            newInst.metatags.sort((a,b) => {
                                 return b.priority - a.priority;
                             });

                            /* Map lectures to new format */

                            for (let lec of inst.lectures) {
                                newInst.lectures.push({
                                    start : lec.start,
                                    end : lec.end,
                                    location : lec.location,
                                    location_url : lec.location_url,
                                    description : lec.description,
                                    lec_key : lec.lec_key,
                                });
                            }
                            newInst.lectures.sort((a,b) => a.start - b.start || a.end - b.start);

                            fs.writeFile("instances/" + newInst.id + ".json", JSON.stringify(newInst), () => {});
                        }
                        fs.writeFile("courses/" + newCourse.id + ".json", JSON.stringify(newCourse), () => {});
                    }
                    fs.writeFile("modules/" + newMod.id + ".json", JSON.stringify(newMod), () => {});
                }
                fs.writeFile("branches/" + newBranch.id + ".json", JSON.stringify(newBranch), () => {});
            }
            newProg.terms.push(newTerm);
        }
        generatedPrograms.push(newProg);
    }

    fs.writeFile("programs/all-programs.json", JSON.stringify(generatedPrograms), () => {});

    console.log("Done");


});
