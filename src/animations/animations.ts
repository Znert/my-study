import { trigger, state, style, animate, transition } from '@angular/animations';

/**
 * This file extracts often used animations for better reuse.
 */

export const fadeInOut = (time : number = 200, scale : number = .9) => {
    return trigger('fadeInOut', [
        transition(':enter', [
            style({
                opacity : 0,
                transform : 'scale(' + scale + ')',
            }),
            animate(time + 'ms ease'),
        ]),
        transition(':leave', [
            animate(time + 'ms ease', style({
                opacity : 0,
                transform : 'scale(' + scale + ')',
            })),
        ])
    ]);
}

export const fadeOut = (time : number = 200, scale : number = .9) => {
    return trigger('fadeOut', [
        transition(':leave', [
            animate(time + 'ms ease', style({
                opacity : 0,
                transform : 'scale(' + scale + ')',
            })),
        ])
    ]);
}

export const growInOut = (time : number = 200) => {
    return trigger('growInOut', [
        transition(':enter', [
            style({
                height : 0
            }),
            animate(time + 'ms ease', style({
                height : '*',
            })),
        ]),
        transition(':leave', [
            animate(time + 'ms ease', style({
                height: 0,
            }))
        ])
    ])
}

export const slideOutRight = (time : number = 200) => {
    return trigger('slideOutRight', [
        transition(':leave', [
            animate(time + 'ms ease', style({
                transform : "translateX(100%)",
            }))
        ])
    ])
}

export const delayDestroy = (time : number = 200) => {
    return trigger('delayDestroy', [
        transition(':leave', [
            animate(time),
        ])
    ])
}

export const scaleInOut = (time : number = 200) => {
    return trigger('scaleInOut', [
        transition(':enter', [
            style({
                transform : 'scale(0)',
            }),
            animate(time + 'ms ease'),
        ]),
        transition(':leave', [
            animate(time + 'ms ease', style({
                transform : 'scale(0)',
            }))
        ])
    ]
    )
}
