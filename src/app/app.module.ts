import { Component, NgModule, VERSION } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MyStudyModule } from '../components/my-study/my-study.module';
import { InstancePickerModule } from '../components/instance-picker/instance-picker.module';
import { CustomPipesModule } from '../pipes/pipes.module';

import { UserService, CategoryService, ProgramService, BranchService, ModuleService, CourseService, InstanceService, DemoService } from '../services/index';

import { RouterModule, Routes } from '@angular/router';

import { MyStudyCompoent } from '../components/my-study/my-study.component';
import { InstancePickerComponent } from '../components/instance-picker/instance-picker.component';

// Routes for navigating the app
const appRoutes : Routes = [
    { path : 'my-study', component : MyStudyCompoent },
    { path : 'instance-picker', component : InstancePickerComponent },
    { path : '', component : MyStudyCompoent, pathMatch : 'full' },
]

/**
 * The main module for this app.
 *
 * It provides all main services and exports the app.
 */
@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        MyStudyModule,
        InstancePickerModule,
        CustomPipesModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule.forRoot(
            appRoutes,
        ),
    ],
    declarations: [
        AppComponent,
    ],
    exports : [
        AppComponent,
    ],
    bootstrap: [
        AppComponent,
    ],
    providers: [
        UserService,
        CategoryService,
        ProgramService,
        BranchService,
        ModuleService,
        CourseService,
        InstanceService,
        DemoService,
    ]
})
export class AppModule {}
