//our root app component
import { Component, NgModule, VERSION } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DemoService } from '../services/index';
import { RerunPipesComp } from '../types/index';

import { growInOut } from '../animations/animations';

/**
 * The main component of this app.
 *
 * It provides the dev buttons as well as the footer and an entry point for the
 * other components.
 *
 * Currently there are two routed components:
 *     * MyStudyCompoent
 *     * InstancePickerComponent
 *
 * This component also provides simulating the passing of a term as well as
 * loading a demo mode for presentations.
 */
@Component({
    selector : 'my-app',
    templateUrl : 'src/app/app.component.html',
    styleUrls : ['src/app/app.component.css'],
    animations : [
        growInOut(),
    ]
})
export class AppComponent {

    // Private variable for generating the buttons
    private routes : { path : string, name : string }[] = [
        { path : '/', name : 'Mein Studium' },
        { path : '/instance-picker', name : 'LVAs auswählen' },
    ];

    // Variable to toggle meta info and developer buttons
    private showMeta = false;

    // Reference to the routed component
    private componentRef : RerunPipesComp | undefined;

    constructor(
        private demoService : DemoService,
    ) {
    }

    /**
     * This method is called, when the demo mode button is pressed.
     *
     * In demo mode, the current user is reset and predefined categories and
     * courses will be set.
     */
    private runDemo() {
        this.demoService.loadDemo();
    }

    /**
     * This method is called, when the +1 term button is pressed.
     *
     * All courses currently in an mandatory category will be completed and a
     * new empty category will be created.
     */
    private simulateTerm() {
        this.demoService.simulateTerm(this.componentRef ? [this.componentRef] : []);
    }

    /**
     * This method will keep the copyright current.
     * @return The year representation to show in the copyright footer.
     */
    private copyRightString : string;
    private getCopyRight() {
        if (this.copyRightString) {
            return this.copyRightString;
        }
        return (this.copyRightString = new Date().getFullYear() != 2017 ? '-' + new Date().getFullYear() : '');
    }

    /**
     * This method will be called after a component was routed.
     * @param  comp The newly routed component.
     */
    private onActivate(comp : any) {
        if (comp instanceof RerunPipesComp) {
            this.componentRef = comp;
        } else {
            this.componentRef = undefined;
        }
    }

}
