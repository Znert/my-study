//main entry point
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from './app/app.module';

// This file does not enbale prod mode for better debugging and to make it
// easier to track the innter working of this app

platformBrowserDynamic().bootstrapModule(AppModule)
