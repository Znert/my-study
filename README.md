# My Study

This repository contains the prototype for the bachelor theses of Simon Hayden. It is used to provide an alternative
UI/UX for the _TISS_ system currently used by the TU Vienna as front and back end.

The prototype focuses on students and how they plan their study. In particular, a design proposal is made on how to
redesign the "Favorites" tab, which is the main focus of many students during the semester.

## Starting the app

If you just want to see the app, you can visit [the gilab.io site](https://znert.gitlab.io/my-study).

If you want to run it yourself, keep on reading!

This prototype uses a pure JIT comipliation, so all you have to do to start the application is start a server to
serve the files. All node_modules will be loaded via a CDN, so you don't have to install anything yourself.

A simple python script will start the required server:

```
python server.py
```

This will launch a simple server (on port 6100), that allows angular routing to work properly.

However, if your browser allows it (Firefox used to work, but recent versions seem to prevent it too), you can also try to just open `index.html`.

Most browsers premit sources to be loaded from another path, because it is seen as XSS.

## Previous Studies

The design idea for this prototype is based on the findings from [Peter Purgathofer](http://igw.tuwien.ac.at/hci/people/ppurgathofer)'s
project for evaluating the _TISS_ platform as a whole. During the research phase it became very clear, that students
wish the planning tools for their studies to be improved.

Although design guidelines where provided, "show" is always better than "tell". For this reason this projects exists.

The (german) previous study can be found [here](https://peter-purgathofer.squarespace.com/tiss-design-report).

## The Prototype

The prototype consists of three parts:
1. **Developer tools** for switching the current component and activating demo mode as well as simulating a term passing.

    Those tools can be accessed by clicking the arrow down button at the top left corner.

2. **Picker** for instances. This site is a simple drop-down menu so that users can select courses they are interested in.

    The data used for the prototype was kindly provided by [_plan.uni_](https://plan.university/) and is comprised of
    the studies _Media Informatics_, _Software and Information Engineering_, _Electrical Engineering_ as well as
    _Architecture_ for the summer and winter term 2017.

3. **"My Study"** which is the culprit of this prototype. The name is intentionally changed from the known "Favorites"
    as the old name suggests that users can announce their preferences instead of managing their university career.

    In this tab users can manage their study. The main idea is to allow users to better group the lectures and support
    the most common uses of the "Favorites" tab.
